#ifndef __SQLITE3_H_INCLUDED__
#define __SQLITE3_H_INCLUDED__

#include <string>
#include <sqlite3.h>

class
Sqlite3
{
  public:
  Sqlite3();
  ~Sqlite3();
  static bool doesDBExist(std::string, sqlite3*&);
  static bool openDB(std::string, sqlite3*&);
  static bool closeDB(sqlite3*&);
  static bool createDB(std::string, sqlite3*&);
  static bool doesTableExist(std::string, sqlite3*&);
  static bool insertQuery(sqlite3*&, std::string);
  static bool insertQueryAndGetResults(sqlite3*&, std::string, sqlite3_stmt*&);
};

#endif // __SQLITE3_H_INCLUDED__
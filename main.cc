/*
http://www.lemoda.net/c/sqlite-select/
http://www.sqlite.org/cintro.html
http://www.yolinux.com/TUTORIALS/SQLite.html
http://www.tutorialspoint.com/sqlite/sqlite_drop_table.htm
http://www.askingbox.com/info/sqlite-creating-an-index-on-one-or-more-columns
*/

/*
Rules are needed by any community virtual or not, but there is the theory and there is the practice. The law (in my country) says it's forbidden to cross the street out of a pedestrian crossing, a police officer in theory can gives fines for this, but in practice they never do this, they cross the street like anybody else.
	

*/


// mettre certaines fonction en inline 
// faire un système de recherche basé sur les resultats de la recherche précédente. Comme ça on peut commencer par des tags puis par location...

#include "Sqlite3.hh"
#include "datatable_manager.hh"

#include <iostream>
#include <sqlite3.h>
#include <stdlib.h>
#include <cstring>
#include <string>
#include <vector>
//#include <gtk/gtk.h>
#include <gtkmm.h>

/*
g++ datatable_manager.cc Sqlite3.cc main.cc -lsqlite3 -std=c++11 `pkg-config gtk+-3.0 --libs --cflags`
*/

//http://blog.borovsak.si/2009/09/glade3-tutorial-2-constructing.html
//http://thegreenapplesite.com/Personal/Glade/T001/Glade_001.html
//https://lazka.github.io/pgi-docs/index.html
//http://stackoverflow.com/questions/8362344/using-the-filechooser-dialog-with-glade
//http://2007.pagesperso-orange.fr/Code/C/GTK/docs/callbacks/g_signal_connect.c.htm
//http://www.gnu.org/software/libc/manual/html_node/index.html#Top


using namespace std;

Gtk::Window* pMainWindow = 0;
Gtk::FileChooserDialog* pOpenDatabaseDialog = 0;
Gtk::ToolButton* pQuit = 0;
Gtk::ToggleToolButton* pToDatabaseManagement = 0;
Gtk::Button* pCancelOpenDatabase = 0;
Gtk::Button* pOpenDatabase = 0;
Gtk::ButtonBox* pButtonBox=0;

/*Database(s) management*/
Gtk::Window *pDatabaseManagement = 0;
Gtk::Label *pDatabaseLabel = 0;
Gtk::ToolButton* pDMOpenDatabase = 0;
Gtk::ToolButton *pDMCloseDatabase = 0;
Gtk::ToolButton *pDMPopulate = nullptr;
Gtk::Label *pNbrOfEntriesLabel = 0;

DataTable_manager *dataTable_manager = 0;
std::string workingDirectory = ".";

static
void onButtonQuit()
{
  if(pMainWindow)
    pMainWindow->close();
}

static
void onButtonCancelOpenDatabase()
{
/* 
  if(pOpenDatabaseDialog)
    pOpenDatabaseDialog->close();
*/
}

static
void onButtonOpenDatabase()
{
/*
  std::string filename = "./bedrock.db";

  dataTable_manager = new DataTable_manager(filename);
  
  pDatabaseLabel->set_label( ("database loaded :"+filename).c_str() );

  pNbrOfEntriesLabel->set_label( ( std::to_string(dataTable_manager->getDataTableCounter())+" entries" ).c_str() );

  pDMPopulate->set_sensitive(true);
*/

}

static
void onButtonOpenDatabaseDialog()
{
  int i = pOpenDatabaseDialog->run();
  std::cout << i << std::endl;

  if(i==0)
  {
    std::string filename = pOpenDatabaseDialog->get_filename();
    std::cout << filename << std::endl;
  }
}

static
void onButtonCloseDatabase()
{
  if(dataTable_manager)
  {
    delete dataTable_manager;
    dataTable_manager = nullptr;
    pDatabaseLabel->set_label( "database closed" );
    pNbrOfEntriesLabel->set_label("# of entries");
  }
  pDMPopulate->set_sensitive(false);
}

static
void onButtonDatabasesManagement()
{
  if(pToDatabaseManagement->get_active())
    pDatabaseManagement->show();
  else 
    pDatabaseManagement->close();
}

static void ContinueTopopulate(const std::string &);


static
void onButtonDMpopulate()
{
  GDir *dir=nullptr;
  GError *error=nullptr;
  const gchar *filename=nullptr;
  gchar* pathPlusFilename=nullptr;

  dir = g_dir_open(workingDirectory.c_str(), 0, &error);
  if( error )
  {
    std::cerr << error->message << std::endl;
    g_clear_error(&error);
  }

  while( filename = g_dir_read_name(dir) )
  {
    pathPlusFilename = g_build_filename(workingDirectory.c_str(),filename,(gchar*)NULL);
    if( g_file_test(pathPlusFilename ,G_FILE_TEST_IS_REGULAR) )
    {
      dataTable_manager->addEntryToDataTable( std::string(filename) , workingDirectory );
    }
    else
    {
      if( g_file_test(pathPlusFilename ,G_FILE_TEST_IS_DIR) )
      {
        ContinueTopopulate(pathPlusFilename);
      }
    }
  }
  g_free(pathPlusFilename);
  g_dir_close(dir);

  pNbrOfEntriesLabel->set_label( ( std::to_string(dataTable_manager->getDataTableCounter())+" entries" ).c_str() );
}

static
void ContinueTopopulate(const std::string &target)
{
  GDir *dir=nullptr;
  GError *error=nullptr;
  const gchar *filename=nullptr;
  gchar* pathPlusFilename=nullptr;
  const std::vector <std::string> VIDE;

  dir = g_dir_open(target.c_str(), 0, &error);
  if( error )
  {
    std::cerr << error->message << std::endl;
    g_clear_error(&error);
  }

  while( filename = g_dir_read_name(dir) )
  {
    pathPlusFilename = g_build_filename(target.c_str(),filename,(gchar*)NULL);
    if( g_file_test(pathPlusFilename ,G_FILE_TEST_IS_REGULAR) )
      dataTable_manager->addEntryToDataTable( std::string(filename) ,target);
    else
      if( g_file_test(pathPlusFilename ,G_FILE_TEST_IS_DIR) )
        ContinueTopopulate(pathPlusFilename);
  }
  g_free(pathPlusFilename);
  g_dir_close(dir);

}

// not gtk stuff, should be nested somewhere else...
static
void ReadFilesInDirectory(const std::string &target)
{
  GDir *dir=nullptr;
  GError *error=nullptr;
  const gchar *filename=nullptr;
  gchar* pathPlusFilename=nullptr;

  dir = g_dir_open(target.c_str(), 0, &error);
  if( error )
  {
    std::cerr << error->message << std::endl;
    g_clear_error(&error);
  }

  while( filename = g_dir_read_name(dir) )
  {
    pathPlusFilename = g_build_filename(target.c_str(),filename,(gchar*)NULL);
    if( g_file_test(pathPlusFilename ,G_FILE_TEST_IS_REGULAR) )
    {
//        std::cout << "(file)      " << pathPlusFilename << std::endl;
    }
    else
    {
      if( g_file_test(pathPlusFilename ,G_FILE_TEST_IS_DIR) )
      {
//        std::cout << "(directory) " << pathPlusFilename << std::endl;
        ReadFilesInDirectory(pathPlusFilename);
      }
    }
  }
  g_free(pathPlusFilename);
  g_dir_close(dir);
}

void
toCallbackFunctionGIO(GFileMonitor      *monitor
                  ,GFile             *file
                  ,GFile             *other_file
                  ,GFileMonitorEvent event_type
                  ,gpointer          user_data
                  ) 
{
  std::cout << g_file_get_path(file) << std::endl;
}

int main(int argc, char **argv)
{
  Glib::RefPtr<Gtk::Application> app = Gtk::Application::create(argc, argv, "org.gtkmm.example");
  Glib::RefPtr<Gtk::Builder> refBuilder = Gtk::Builder::create();
  refBuilder->add_from_file("GTK-Melechesh.glade");

  // recursively monitoring a directory tree :
  // http://lwn.net/Articles/605128/
  GFile *file = g_file_new_for_path("."); 
  GFileMonitor *monitor;
  monitor = g_file_monitor_directory(file, G_FILE_MONITOR_NONE, nullptr, nullptr);
  g_signal_connect(monitor, "changed", G_CALLBACK (toCallbackFunctionGIO), nullptr);


  /*main window*/
  refBuilder->get_widget("Melechesh's storage organizer", pMainWindow);
  refBuilder->get_widget("TB-quit", pQuit);
  refBuilder->get_widget("TB-DatabasesManagement",pToDatabaseManagement);

  /*Database(s) management*/
  refBuilder->get_widget("Database(s) management",pDatabaseManagement);
  refBuilder->get_widget("DM-DatabaseLabel",pDatabaseLabel);
  refBuilder->get_widget("DM-NbrOfEntriesLabel",pNbrOfEntriesLabel);
  refBuilder->get_widget("DM-CloseDatabase",pDMCloseDatabase);
  refBuilder->get_widget("DM-OpenDatabase", pDMOpenDatabase);
  refBuilder->get_widget("DM-Populate", pDMPopulate);

  /*Open dialog*/
  refBuilder->get_widget("OpenDatabaseDialog", pOpenDatabaseDialog);
  refBuilder->get_widget("CancelOpenDatabase", pCancelOpenDatabase);
  refBuilder->get_widget("OpenDatabase", pOpenDatabase);
  refBuilder->get_widget("filechooserdialog-action_area1", pButtonBox);

  if(pQuit)
    pQuit->signal_clicked().connect( sigc::ptr_fun(onButtonQuit) );

  if(pToDatabaseManagement)
    pToDatabaseManagement->signal_clicked().connect( sigc::ptr_fun(onButtonDatabasesManagement) );

  if(pDMOpenDatabase)
    pDMOpenDatabase->signal_clicked().connect( sigc::ptr_fun(onButtonOpenDatabaseDialog) );

  if(pOpenDatabase)
  {
    pOpenDatabase->signal_clicked().connect( sigc::ptr_fun(onButtonOpenDatabase) );
  }

  if(pCancelOpenDatabase)
  {
    pCancelOpenDatabase->signal_clicked().connect( sigc::ptr_fun(onButtonCancelOpenDatabase) );
  }


  if(pOpenDatabaseDialog)
  {
    Glib::RefPtr<Gtk::FileFilter> filter_text = Gtk::FileFilter::create();
    filter_text->set_name("database *.db");
    filter_text->add_pattern("*.db");
    pOpenDatabaseDialog->add_filter(filter_text);
  }

  if(pDMCloseDatabase)
    pDMCloseDatabase->signal_clicked().connect( sigc::ptr_fun(onButtonCloseDatabase) );

  if(pDMPopulate)
    pDMPopulate->signal_clicked().connect( sigc::ptr_fun( onButtonDMpopulate ) );


/* TESTIN RESEARCH
  std::vector <std::string> test_vector;
  test_vector.push_back("idiot");
  test_vector.push_back("potiche");

//  dataTable_manager.searchByTags(test_vector);
  dataTable_manager.searchByFilename("%tot%.txt");
TESTIN RESEARCH */ 

app->run(*pMainWindow);

exit(0);
}
          

Licence :
=========
  Copyright 2015 Jäger Nicolas
 
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  MA 02110-1301, USA.

Overview :
==========
 ***«Melechesh»*** is a program to help to tidy up a disk . It's written
  for working on **linux**. It's far from being usable, I don't even know
  myself what strategy I should use... I'm starting to write this program
  because I have several disks, with many files stored over years.
  
Dependecies :
==============
  - gtkmm
  - SQlite3

Compil :
========
  - $ ./compile.cmd

Known bug :
============
  - Alfred the beetle
  
About the name :
================
  *«Melechesh»* is a fucking awesome metal band! 
  http://www.metal-archives.com/bands/Melechesh/424

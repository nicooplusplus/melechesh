#include "Sqlite3.hh"

#include <iostream>
#include <stdlib.h>
#include <fstream>



Sqlite3::Sqlite3()
{

}

Sqlite3::~Sqlite3()
{

}

bool
Sqlite3::doesDBExist(std::string filename, sqlite3* &db)
{
  std::ifstream file ( filename.c_str() );  
  if( file )
    return true;
  else
    return false; 
}

bool
Sqlite3::openDB(std::string filename, sqlite3* &db)
{
  std::ifstream file ( filename.c_str() );  
  if( !file )
  {
    std::cerr << "Can't open database, file not found. " << std::endl;
    return false;
  }
  file.close();

  if( sqlite3_open(filename.c_str(), &db) )
  {
    std::cerr << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
    sqlite3_close(db);
    return false;
  }

  return true;
}


// create and it opens it too..
bool
Sqlite3::createDB(std::string filename, sqlite3* &db)
{
  std::ifstream file ( filename.c_str() );  
  if( file )
  {
    std::cerr << "Can't create database, file already exists. " << std::endl;
    exit(1);
  }

  if( sqlite3_open(filename.c_str(), &db) )
  {
    std::cerr << "Can't create database, problem unknown: " << sqlite3_errmsg(db) << std::endl;
    sqlite3_close(db);
    exit(1);
  }
  return true;
}

bool
Sqlite3::closeDB(sqlite3* &db)
{
  sqlite3_close(db);
}

bool
Sqlite3::doesTableExist(std::string tableName, sqlite3* &db)
{
  sqlite3_stmt *stmt;
  //std::cout << ("SELECT count(type) FROM sqlite_master WHERE type='table' AND name='"+tableName+"'").c_str() << std::endl;

  if( sqlite3_prepare_v2(db, ("SELECT count(type) FROM sqlite_master WHERE type='table' AND name='"+tableName+"'").c_str(), -1, &stmt, NULL) != SQLITE_OK )
  {
    std::cerr << "Unexpected error #0 from Sqlite3::isTableExists : " << sqlite3_errmsg(db) << std::endl;
    exit(1);
  }

  int i = sqlite3_step(stmt);
  bool result =  ((bool)sqlite3_column_int(stmt, 0)) ;

  while(i!=SQLITE_DONE)
    i = sqlite3_step(stmt);

  return result;
}

bool
Sqlite3::insertQuery(sqlite3* &db, std::string query )
{
  sqlite3_stmt *stmt;
  //std::cout << query << std::endl;

  if( sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, NULL ) != SQLITE_OK )
  {
    std::cerr << "Unexpected error from Sqlite3::insertQuery : " << sqlite3_errmsg(db) << std::endl;
    exit(1);
  }

  int i;
  while(i != SQLITE_DONE)
    i = sqlite3_step(stmt);
 
  if( i == 101 )
  {
    return true;
  }
  else
  {
    std::cout <<"Unexpected code return from Sqlite3::insertQuery : "<< i <<std::endl;
    return false;
  }
}

bool
Sqlite3::insertQueryAndGetResults(sqlite3* &db, std::string query, sqlite3_stmt* &stmt )
{
  //std::cout << query << std::endl;
  if( sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, NULL ) != SQLITE_OK )
  {
    std::cerr << "Unexpected error from Sqlite3::insertQueryAndGetResults : " << sqlite3_errmsg(db) << std::endl;
    exit(1);
  }
  return true;
}

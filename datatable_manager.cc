#include "datatable_manager.hh"

#include "Sqlite3.hh"
#include <iostream>
#include <stdlib.h>
#include <fstream>

DataTable_manager::DataTable_manager(const std::string &database_)
:database(nullptr)
{
  if( Sqlite3::doesDBExist( database_ ,database ) )
  {
    if( !Sqlite3::openDB( database_ ,database ) )
      exit(1);
  }
  else
  {
    if( !Sqlite3::createDB( database_ ,database ) )
      exit(1);
  }

  if( !this->doesDataTableExist() )
    if( !this->createDataTable() )
      exit(1);
}

DataTable_manager::~DataTable_manager()
{
  Sqlite3::closeDB(database);
}

bool
DataTable_manager::doesDataTableExist()
{
  return Sqlite3::doesTableExist("__DATA_TABLE__", database);
}

bool
DataTable_manager::doesResultTableExist()
{
  return Sqlite3::doesTableExist("__RESULT_TABLE__", database);
}

bool
DataTable_manager::doesTagTableExist(const std::string &tag)
{
  return Sqlite3::doesTableExist("__TAG_"+tag+"__", database);
}


bool
DataTable_manager::createDataTable()
{
  return ( Sqlite3::insertQuery(database,"CREATE TABLE IF NOT EXISTS __DATA_TABLE__ (FILE_ID INTEGER PRIMARY KEY AUTOINCREMENT, FILENAME TEXT NOT NULL, LOCATION TEXT NOT NULL, DESCRIPTION TEXT)")
        && Sqlite3::insertQuery(database,"CREATE TABLE __COUNTER__ (__DATA_TABLE_ENTRIES__ INT)")
        && Sqlite3::insertQuery(database,"INSERT INTO __COUNTER__ (__DATA_TABLE_ENTRIES__) VALUES (0)")  // init the counter to 0
        && Sqlite3::insertQuery(database,"CREATE TRIGGER __DATA_TABLE_ENTRIES__TRIGGER__ AFTER INSERT ON __DATA_TABLE__ BEGIN UPDATE __COUNTER__ SET __DATA_TABLE_ENTRIES__=__DATA_TABLE_ENTRIES__+1; END;") // trigger who increments the counter each time a new insert is done in __DATA_TABLE__
         );
}

bool
DataTable_manager::createNewResultTable()
{
  return Sqlite3::insertQuery(database,"CREATE TABLE IF NOT EXISTS __RESULT_TABLE__ (FILE_ID INT NOT NULL, FILE_SCORE INT)");
}

bool
DataTable_manager::deleteOldResultTable()
{
  return Sqlite3::insertQuery(database,"DROP TABLE IF EXISTS __RESULT_TABLE__");
}

bool
DataTable_manager::createTagTable(const std::string &tag)
{
  return Sqlite3::insertQuery(database,"CREATE TABLE IF NOT EXISTS __TAG_"+tag+"__ (FILE_ID INTEGER PRIMARY KEY)");
}

bool
DataTable_manager::doesEntryExist(const std::string &entry /*, std::string location, std::string description...*/)
{
  sqlite3_stmt *stmt;
  Sqlite3::insertQueryAndGetResults(database, "SELECT * FROM __DATA_TABLE__ WHERE FILENAME = '"+entry+"'", stmt);

  if( sqlite3_step(stmt) ==  SQLITE_DONE )
    return false;

  return true;
}

bool
DataTable_manager::doesResultExist(const std::string &entry)
{
  sqlite3_stmt *stmt;
  Sqlite3::insertQueryAndGetResults(database, "SELECT * FROM __RESULT_TABLE__ WHERE FILE_ID = '"+entry+"'", stmt);

  if( sqlite3_step(stmt) ==  SQLITE_DONE )
    return false;

  return true;
}

bool
DataTable_manager::addEntryToDataTable(const std::string &filename, const std::string &location)
{
  if( doesEntryExist(filename) )
    return false;

  return Sqlite3::insertQuery(database,"INSERT INTO __DATA_TABLE__ (FILENAME, LOCATION) VALUES ('"+filename+"', '"+location+"')");
}

bool
DataTable_manager::deleteEntryFromDataTable(const std::string &filename)
{
  if( !doesEntryExist(filename) )
    return false;

  return Sqlite3::insertQuery(database,"DELETE FROM __DATA_TABLE__ WHERE (FILENAME) VALUES ('"+filename+"')") ;
}

bool
DataTable_manager::synchronizeEntry(const std::string &filename, const std::string &location)
{
  std::ifstream file ( (location+"/"+filename).c_str() );

  if( doesEntryExist(filename) && !file)
    deleteEntryFromDataTable(filename); // mettre un trigger pour le compteur
  if( !doesEntryExist(filename) && file)
    addEntryToDataTable(filename, location);
}

int
DataTable_manager::getDataTableCounter()
{
  sqlite3_stmt *stmt;
  Sqlite3::insertQueryAndGetResults(database, "SELECT __DATA_TABLE_ENTRIES__ FROM __COUNTER__", stmt);
  sqlite3_step(stmt);

  return (int)sqlite3_column_int (stmt, 0);
}

bool
DataTable_manager::addTags( const std::vector <std::string> &list)
{
  for (auto & tag : list)
  {
    if( doesTagTableExist(tag))
    {
      if( Sqlite3::insertQuery(database,"INSERT INTO __TAG_"+tag+"__ (FILE_ID) VALUES ('"+std::to_string(getDataTableCounter())+"')") )
      {
        continue;
      }
    }
    else
    {
      if( createTagTable(tag) )
      {
        if( Sqlite3::insertQuery(database,"INSERT INTO __TAG_"+tag+"__ (FILE_ID) VALUES ('"+std::to_string(getDataTableCounter())+"')") )
        {
          continue;
        }
      }
    }
    return false;
  }
  return true;
}

bool
DataTable_manager::searchByTags(const std::vector <std::string> &list)
{
  sqlite3_stmt *stmt;

  deleteOldResultTable();
  createNewResultTable();

  for (auto & tag : list)
  {
    if( !doesTagTableExist(tag) )
      continue;

    Sqlite3::insertQueryAndGetResults(database, "SELECT FILE_ID FROM __TAG_"+tag+"__", stmt);

    while( sqlite3_step(stmt) == SQLITE_ROW )
    {
      std::string result;
      result.append(reinterpret_cast<const char*>( sqlite3_column_text (stmt, 0) ));
      if( doesResultExist(result) )
        Sqlite3::insertQuery(database,"UPDATE __RESULT_TABLE__ SET FILE_SCORE=FILE_SCORE+1 WHERE FILE_ID="+result);
      else
        Sqlite3::insertQuery(database,"INSERT INTO __RESULT_TABLE__(FILE_ID, FILE_SCORE) VALUES ("+result+", 1)");
    }
  }

  Sqlite3::insertQuery(database,"SELECT * FROM __RESULT_TABLE__ ORDER BY FILE_SCORE DESC" );
}

bool
DataTable_manager::searchByFilename(const std::string &query)
{
  sqlite3_stmt *stmt;

  deleteOldResultTable();
  createNewResultTable();

  // one can use "%" and "_"...
  Sqlite3::insertQueryAndGetResults(database, "SELECT FILE_ID FROM __DATA_TABLE__ WHERE FILENAME LIKE '"+query+"'", stmt);

  while( sqlite3_step(stmt) == SQLITE_ROW )
  {
    std::cout << sqlite3_column_text (stmt, 0) << std::endl;
  }
}
#ifndef __DATATABLE_MANAGER_H_INCLUDED__
#define __DATATABLE_MANAGER_H_INCLUDED__

#include <vector>
#include <string>
#include <sqlite3.h>

class
DataTable_manager
{
  public:
  DataTable_manager(const std::string &);
  ~DataTable_manager();
  bool doesDataTableExist();
  bool doesResultTableExist();
  bool doesTagTableExist(const std::string &);
  bool createDataTable();
  bool createNewResultTable();
  bool deleteOldResultTable();
  bool createTagTable(const std::string &);
  bool doesEntryExist(const std::string &);
  bool doesResultExist(const std::string &);
  bool addEntryToDataTable(const std::string&, const std::string&);
  bool deleteEntryFromDataTable(const std::string&);
  bool synchronizeEntry(const std::string&, const std::string&);
  int getDataTableCounter();
  bool addTags(const std::vector <std::string>&);
  bool searchByTags(const std::vector <std::string>&);
  bool searchByFilename(const std::string &);

  private:
  sqlite3 * database;

};

#endif // __TAGS_DATATABLE_H_INCLUDED__